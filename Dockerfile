FROM java:8

WORKDIR /javacard
COPY java_card_kit-2_2_2-linux.zip .
RUN unzip java_card_kit-2_2_2-linux.zip

WORKDIR java_card_kit-2_2_2
RUN unzip java_card_kit-2_2_2-rr-bin-linux-do.zip

ENV JC_HOME /javacard/java_card_kit-2_2_2

RUN apt-get update && apt-get install -y --no-install-recommends ant && rm -rf /var/lib/apt/lists/*
